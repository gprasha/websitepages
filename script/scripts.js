jQuery(document).ready(function () {
    var baseurl = "http://localhost/taxibay/";
    //var baseurl = "http://bigappcompany.in/demos/taxibay/";
    var checkurl = window.location.href;
    if (checkurl == baseurl || checkurl == baseurl + "index.html") {
        $("body").addClass("activeloader");
        setTimeout(function () {
            $(".loader").fadeOut(1000, function () {
                $("body").removeClass("activeloader");
            });
        }, 2000);

        setTimeout(function () {
            jQuery(".HomeScene1-dcross-line1").addClass("show");
        }, 3200);
        setTimeout(function () {
            jQuery(".HomeScene1-dcross-line2").addClass("show");
        }, 4200);

        $('.testimonialslider').bxSlider({
            mode: 'vertical',
            pager: false,
            auto: true
        });
        $(".customersection .bx-controls-direction a.bx-prev").html('<i class="fa fa-angle-up" aria-hidden="true"></i>');
        $(".customersection .bx-controls-direction a.bx-next").html('<i class="fa fa-angle-down" aria-hidden="true"></i>');
    }
    var checkwidth = $(window).width();
    if (checkwidth < 768) {
        $(".openmenu_js").click(function () {
            $(".openmenu_js i").toggleClass("fa-bars fa-close");
            $(".mainmenu ").toggleClass("active");
            $(".overlayformobile ").toggleClass("active");
            $("body").toggleClass("overflowhidden");
        });

        $(".overlayformobile").click(function () {
            $(".mainmenu ").removeClass("active");
            $(".overlayformobile ").removeClass("active");
            $(".openmenu_js i").toggleClass("fa-bars fa-close");
            $("body").toggleClass("overflowhidden");
        });
    }
    if (checkwidth < 640) {
        $(".quicklinks h5").click(function () {
            $(this).next().slideToggle(500);
            $(this).toggleClass("active");
        });
    }
   
    if (checkurl == baseurl + "outstation.html" || checkurl == baseurl + "local-booking.html") {
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        $('#pickupdate,#dropdate,#pickupdateforlocalpackage').fdatepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            },
            format: 'dd-mm-yyyy hh:ii',
            disableDblClickSelection: true,
            leftArrow: '<<',
            rightArrow: '>>',
            pickTime: true
        });
       

//        $("#pickuplocation").focus();
//        $("#pickuplocation").blur(function () {
//            var getpickuplocation = $(this).val();
//            if (getpickuplocation == "") {
//                alertify.error("Please Enter The Pickup Location");
//                $(this).focus();
//            }
//        });

        $("#destinationlocation").blur(function () {
            var getdestination = $(this).val();
            if (getdestination == "") {
                alertify.error("Please Enter The Destination");
                $(this).focus();
            }
        });


        $("#pickuppoint").blur(function () {
            var pickuppoint = $(this).val();
            if (pickuppoint == "") {
                alertify.error("Please Enter The Pickup Point");
                $(this).focus();
                $("#booking-form-dates").slideUp(500);
                $(".continuetonextstep  button").addClass("blockpointerevents");
            } else {
                $("#booking-form-dates").slideDown(500);
                $(".continuetonextstep  button").removeClass("blockpointerevents");
            }
        });
        
    }
 if(checkurl == baseurl+"outstation.html" || checkurl == baseurl+"local-booking.html"){
        $(".jq").jqTransform();
    }
    if (checkurl == baseurl + "outstation-people-travelling.html") {
        $("#slider-1").slider({
            range: "max",
            min: 0,
            max: 50,
            value: 10,
            step: 1,
            change: function (event, ui) {
                $("#num_pax").val(ui.value);
            },
            slide: function (event, ui) {
                $("#num_pax").val(ui.value);
            }
        });

        $('#num_pax').on('keyup', function (event) {
            $("#slider-1").slider("value", $("#num_pax").val());
        });
        $('#num_pax').on('change', function (event) {
            $("#slider-1").slider("value", $("#num_pax").val());
        });

        $("#num_pax").val($("#slider-1").slider("value"));

        $(".sliderlevels ul li a").click(function () {
            let getpointervalues = $(this).attr('data-slidervalue');
            $('#num_pax').val(getpointervalues).trigger('keyup')
        });

        $("#num_pax").on("keyup", function () {
            let getnoofpassengers = $(this).val();
            if (getnoofpassengers == "" || getnoofpassengers == 0) {
                alertify.error("Please Enter The No of Passengers");
                $(".continuetonextstep button").addClass("blockpointerevents");
            } else {
                $(".continuetonextstep button").removeClass("blockpointerevents");
            }
        });

    }
    if (checkurl == baseurl + "select-vehicle.html" || checkurl == baseurl+"select-vehicle-local.html") {
        $(".vehiclediv").click(function () {
            let vehicletype = $(this).attr("data-vehicle-type");
            if (vehicletype == "Sedan" || vehicletype == "Hatchback" || vehicletype == "MUV") {
                $(".passengerslider").slideUp(500);
            } else {
                $(".passengerslider").slideDown(500);

            }
        });

        $(".vehiclediv").click(function () {
            $(".vehiclediv").removeClass("active");
            $(this).addClass("active");
            $(".continuetonextstep button").removeClass("blockpointerevents");
        });
        $("#slider-1").slider({
            range: "max",
            min: 0,
            max: 50,
            value: 10,
            step: 1,
            change: function (event, ui) {
                $("#num_pax").val(ui.value);
            },
            slide: function (event, ui) {
                $("#num_pax").val(ui.value);
            }
        });

        $('#num_pax').on('keyup', function (event) {
            $("#slider-1").slider("value", $("#num_pax").val());
        });
        $('#num_pax').on('change', function (event) {
            $("#slider-1").slider("value", $("#num_pax").val());
        });

        $("#num_pax").val($("#slider-1").slider("value"));

        $(".sliderlevels ul li a").click(function () {
            let getpointervalues = $(this).attr('data-slidervalue');
            $('#num_pax').val(getpointervalues).trigger('keyup');
            let getnoofpassengers = $("#num_pax").val();
            if (getnoofpassengers == 0 || getnoofpassengers == "") {
                alertify.error("Please Enter The No of Passengers");
                $(".continuetonextstep button").addClass("blockpointerevents");
            } else {
                $(".continuetonextstep button").removeClass("blockpointerevents");
            }
        });

        $("#num_pax").on("keyup", function () {
            let getnoofpassengers = $(this).val();
            if (getnoofpassengers == "" || getnoofpassengers == 0) {
                alertify.error("Please Enter The No of Passengers");
                $(".continuetonextstep button").addClass("blockpointerevents");
            } else {
                $(".continuetonextstep button").removeClass("blockpointerevents");
            }
        });



        if (checkwidth < 992) {
            jQuery('.vehicletypeslider').bxSlider({
                minSlides: 1,
                maxSlides: 8,
                slideWidth: 148,
                infiniteLoop: false,
                hideControlOnEnd: true,
                slideMargin: 10,
                pager: false
            });
        }


    }

    if (checkurl == baseurl + "vehicle-type.html" || checkurl ==  baseurl+"vehicle-type-local.html") {
        $(".vehicletypediv ").click(function () {
            $(".vehicletypediv").removeClass("active");
            $(this).addClass("active");
            $('.continuetonextstep  button').removeClass("blockpointerevents");
        });
    }

    if (checkurl == baseurl + "booking-summary.html" || checkurl == baseurl + "booking-summary-local.html") {
        if (checkwidth < 992) {
            jQuery('.vehicletypeslider').bxSlider({
                minSlides: 1,
                maxSlides: 8,
                slideWidth: 148,
                infiniteLoop: false,
                hideControlOnEnd: true,
                slideMargin: 10,
                pager: false
            });
        }

        $('.openeditoptions').on('click', function () {
            var click = +$(this).data('clicks') || 0;
            if (click % 2 == 1) {
                $(".gotosteps").slideUp(500);
                $(this).html("Edit ");
            } else {
                $(".gotosteps").slideDown(500);
                $(this).html("Save");
            };
            $(this).data('clicks', click + 1);
        });

        $(".openquoteform_js").click(function () {
            $("#trip_summary").slideUp(500);
            $(".getquote-form").slideDown(500);
        });

        $(".close_getquote_form_js").click(function () {
            $(".getquote-form").slideUp(500);
            $("#trip_summary").slideDown(500);

        });

        $(".openotpform_js").click(function () {
            $(".otppopupsection ").addClass("active");
        });

        $(".close_getotp_form").click(function () {
            $(".otppopupsection ").removeClass("active");
        });
    }

    if (checkurl == baseurl + "outstation-booking-quotation.html" || checkurl == baseurl + "outstation-booking-quotation-shuttle.html") {
        if (checkwidth < 768) {
            $(".quotes-footer h5").click(function () {
                $(this).next().slideToggle(500);
            });
            $(".quotes-footer ul.nav li a").click(function () {
                $(this).parent().parent().slideUp(500);
            });
        }
    }

    $(".viewtermsnconditions_js").click(function () {
        $(".termsandconditionsshow_js").slideToggle(500);
    });
    $('a.slowscroll[href^="#"]').on('click', function (e) {
        var target = this.hash;
        var $target = jQuery(target);
        jQuery('html, body').stop().animate({
            'scrollTop': $target.offset().top - 0
        }, 700, 'swing', function () {});
        return false;
    });
});